import Vue from 'vue'
import Router from 'vue-router'
import GenerateGraphics from '@/pages/GenerateGraphics'
import NotFound from '@/pages/NotFound'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'GenerateGraphics',
      component: GenerateGraphics,
    },
    {
      path: '(.*)',
      name: 'NotFound',
      component: NotFound,
    },
  ]
})
